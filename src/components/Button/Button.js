import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';
import {Star, Cart} from "../theme/icons";

export default function Button ({id, onClick, image, type, text, filled}) {

           let innerBlock = '';

        switch (type) {
            case "ok":
                innerBlock = text;
                break;
            case "cancel":
                innerBlock = text;
                break;
            case "star":
                innerBlock = (
                    <Star
                        filled={filled}
                    />
                );
                break;
            case "cart":
                innerBlock = (
                    <Cart
                        filled={filled}
                    />);
                break;
        }



        return (
            <div className={"myButton"}
                onClick={() => onClick(id)}
            >

                {innerBlock}
            </div>

        );
    }


Button.propTypes = {
    text: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    color: PropTypes.string,
    buttonClassName: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
    color: 'pink'
};